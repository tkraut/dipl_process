export PATH := bin:$(PATH)
%.features.csv: %.wav
	SMILExtract -C conf/IS13_ComParE.conf -I "$<" -csvoutput "$@"

%.output.csv: %.elan.txt
	elanToFeatures.sh < $< > $@

%.sql.txt: %.sql
	sqlToElan.sh $< > $@
