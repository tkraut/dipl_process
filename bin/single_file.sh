#!/bin/bash

. ../conf/config.inc

ABSFILE=`realpath "$1"`

DIR=`dirname "$ABSFILE"`

FILE=`basename "$ABSFILE"`

NAME=${FILE%.*}

ABSNAME="$DIR/$NAME"


make -f Makefile.data "$DIR/$NAME.csv"

trees.R "$ABSNAME"
