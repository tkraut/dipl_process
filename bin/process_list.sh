#!/bin/bash

. ../conf/config.inc

grep -v '^#' "$1" | while read -r line || [[ -n "$line" ]]; do
	single_file.sh "$DATADIR/$line"
done
