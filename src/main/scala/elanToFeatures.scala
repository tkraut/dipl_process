import java.text.{DecimalFormat, DecimalFormatSymbols}
import java.util.Locale

import com.github.tototoshi.csv.CSVFormat

/**
  * Filter that converts Elan TSV export to TSV readable by R
  * It expects input with following columns (possible values in parentheses):
  * Tier (one of Turns, Words, Emotions)
  * Dummy (no value is expected)
  * From (double)
  * To (double)
  * Duration (double)
  * Value (string)
  *
  * Output has 2 columns
  * Time
  * Annotation
  * Time frames with any value in tier "Emotion" have output annotation 1, all others have 0
  *
  */
object elanToFeatures extends CsvSimpleProcessorApp {

  implicit val csvFormat: CSVFormat = CSVFormats.TSVWithoutDelimiter

  case class Out(time: Double, annotation: Int)

  case class In(tier: String, dummy: Unit, from: Double, to: Double, duration: Double, value: String)

  def read(line: Seq[String]): In = In(
      line(0),
      (),
      line(2).toDouble,
      line(3).toDouble,
      line(4).toDouble,
      line(5)
    )

  private val timeFmt = new DecimalFormat("#.0", DecimalFormatSymbols.getInstance(Locale.US))
  def write(out: Out): Seq[String] = Seq(
    timeFmt.format(out.time),
    out.annotation.toString
  )

  val Turns = "Turns"
  val Words = "Words"
  val Emotions = "Emotions"
  val FrameLength = 0.1

  def transform(in: Iterator[In]): Iterator[Out] = {

    /**
      * Unrolls single (variable length) segment to multiple fixed length frames
      * Segment boundaries are shifted towards nearest lower multiple of frame length
      * @param in Segment to be unrolled
      */
    def unroll(in: In): Seq[Frame] = {
      val begin = in.from - in.from % FrameLength
      val end = in.to - in.to % FrameLength
      begin to end by FrameLength map {
        Frame(_, in.tier, in.value)
      }
    }

    val inRows: List[In] = in.toList

    val speakers: Set[String] = inRows
      .iterator
      .filter(_.tier == Turns)
      .map(_.value)
      .toSet

    val annotMap: Map[Double, List[Frame]] = inRows
      .flatMap(unroll)
      .groupBy(_.time)
    val maxTime = annotMap.keysIterator.max

    val outRows: Iterator[Out] = (0.0 to maxTime by FrameLength)
      .iterator
      .map { time =>
        val a = annotMap.get(time)
          .filter(_.exists(_.tier == Emotions))
          .map(_ => 1)
          .getOrElse(0)
        Out(time, a)
      }

    outRows
  }

  case class Frame(time: Double, tier: String, value: String)
}
