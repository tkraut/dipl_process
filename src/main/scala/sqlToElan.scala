import com.github.tototoshi.csv.CSVFormat

import scala.util.Try

/**
  * Filter that converts Dialog corpus annotation SQL dump to TSV for import to Elan
  * It skips rows with no turn at the beginning
  *
  * Then it collects turns, sentences and words to separate tiers
  *
  * For each contiguous sequence of words with same sentence id there is one record in Sentences tier
  * For each contiguous sequence of words with same turn id there is one record in Turns tier
  * For each word there is one record in Words tier
  *
  * In input time is measured in hundredths of seconds.
  * In output time is measured in seconds
  * Therefore timestamps are divided by a factor of 100
  */
object sqlToElan extends CsvMultiProcessorApp {

  implicit val csvFormat: CSVFormat = CSVFormats.TSVWithoutDelimiter

  val Turns = "Turns"
  val Words = "Words"
  val Sentences = "Sentences"
  val TimeFactor = 100.0

  case class In(
                 from: Option[Int], to: Option[Int],
                 id: Int,
                 word: String, lemma: String, tag: String,
                 isWord: Boolean,
                 speaker: Option[String], recording: String,
                 turnId: Option[Int], wordInTurn: Option[Int], anyInTurn: Option[Int], wordId: Option[Int]
               ) {
    def toWordOpt: Option[Out] = for {
      f <- from
      t <- to
      d <- duration
      if isWord
    } yield Out(
      Words, (),
      f / TimeFactor, t / TimeFactor, d / TimeFactor,
      word
    )

    def duration: Option[Int] = for (f <- from; t <- to) yield t - f

  }

  case class Out(tier: String, dummy: Unit, from: Double, to: Double, duration: Double, value: String)

  override def write(out: Out): Seq[String] = Seq(
    out.tier,
    "",
    out.from.toString,
    out.to.toString,
    out.duration.toString,
    out.value
  )

  def read(line: Seq[String]) = In(
    Try(line(0).toInt).toOption,
    Try(line(1).toInt).toOption,
    line(2).toInt,
    line(3),
    line(4),
    line(5),
    line(6) == ".",
    Some(line(7)).filter(_ != "\\N"),
    line(8),
    Try(line(9).toInt).toOption,
    Try(line(10).toInt).toOption,
    Try(line(11).toInt).toOption,
    Try(line(12).toInt).toOption
  )

  def consume(in: Iterator[In], writeRow: Out => Unit): Unit = {
    case class TurnState(turn: Int, from: Int, to: Int, speaker: String) {
      def toTurn: Out = Out(Turns, (), from / TimeFactor, to / TimeFactor, (to - from) / TimeFactor, speaker)
    }
    object TurnState {
      def fromIn(in: In): TurnState = TurnState(
        in.turnId.get,
        in.from.get,
        in.to.get,
        in.speaker.get
      )
    }
    case class SentenceState(from: Int, to: Int, wordsReverse: List[String]) {
      def isEmpty: Boolean = wordsReverse.isEmpty

      def toSentenceOpt: Option[Out] =
        if (isEmpty) None
        else Some(
          Out(
            Sentences, (),
            from / TimeFactor, to / TimeFactor, (to - from) / TimeFactor,
            wordsReverse.reverse.mkString(" ")
          ))


      def extended(row: In): SentenceState =
        extended(row.word, row.from.get, row.to.get)

      def extended(word: String, from: Int, to: Int): SentenceState =
        copy(
          from = if (isEmpty) from else this.from,
          to = to,
          wordsReverse = word :: wordsReverse
        )
    }
    object SentenceState {
      def fromIn(in: In): SentenceState =
        if (in.isWord) single(in)
        else empty

      def single(in: In): SentenceState =
        SentenceState(
          in.from.get,
          in.to.get,
          List(in.word)
        )

      def empty: SentenceState = SentenceState(-1, -1, List.empty)
    }

    val iter = in
      .dropWhile(_.turnId.isEmpty)

    val first = iter.next()

    var turnState: TurnState = TurnState.fromIn(first)
    var sentenceState: SentenceState = SentenceState.empty

    for (row <- iter) {
      val turnChanged = row.turnId.get != turnState.turn
      if (turnChanged) {
        writeRow(turnState.toTurn)
        turnState = TurnState.fromIn(row)
      } else {
        turnState = turnState.copy(to = row.to.get)
      }

      if (turnChanged || !row.isWord) {
        sentenceState.toSentenceOpt.foreach(writeRow)
        sentenceState = SentenceState.fromIn(row)
      } else {
        sentenceState = sentenceState.extended(row)
      }
      row.toWordOpt.foreach(writeRow)
    }
    sentenceState.toSentenceOpt.foreach(writeRow)
    writeRow(turnState.toTurn)
  }


}
