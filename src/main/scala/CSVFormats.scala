import com.github.tototoshi.csv.{CSVFormat, TSVFormat}

/**
  * @author Tomáš Kraut <tomas.kraut@matfyz.cz>
  */
object CSVFormats {

  /**
    * Tab separated, but no escape char, so single backslash is allowed (quotes may still be escaped by doubling)
    */
  object TSVWithoutDelimiter extends CSVFormat with TSVFormat {
    override val escapeChar = '\u0000'
  }

}
