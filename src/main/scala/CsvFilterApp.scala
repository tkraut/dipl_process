import com.github.tototoshi.csv.{CSVFormat, CSVReader, CSVWriter}

import scala.io.Source

/**
  * Object extending this trait can be used as CSV filter
  * It reads one CSV file from [[System.in]] or file given as first parameter
  * and writes output to [[System.out]]
  *
  * @author Tomáš Kraut <tomas.kraut@matfyz.cz>
  */
trait CsvFilterApp {
  /**
    * This method is entry point for app - input reader and output writer are provided as parameters
    *
    * @param in  Method should read input from this reader
    * @param out Method should write its output to this writer
    */
  def process(in: CSVReader, out: CSVWriter): Unit

  /**
    * Format that is used to read and write data
    */
  implicit val csvFormat: CSVFormat


  /**
    * @param args If empty, read from [[System.in]], otherwise take first parameter and read from file with this name
    */
  def main(args: Array[String]): Unit = {
    val in = args.headOption match {
      case Some(file) => CSVReader.open(file)
      case None       => CSVReader.open(Source.fromInputStream(System.in))
    }

    val out = CSVWriter.open(System.out)
    process(in, out)
  }

}

/**
  * Base trait for more specific CSV processing
  */
trait CsvProcessorApp extends CsvFilterApp {
  type In
  type Out

  def read(line: Seq[String]): In

  def write(out: Out): Seq[String]
}


/**
  * For app that can be written as single transformation
  */
trait CsvSimpleProcessorApp extends CsvProcessorApp {
  def transform(in: Iterator[In]): Iterator[Out]

  def process(in: CSVReader, out: CSVWriter): Unit = {
    transform(in.iterator.map(read))
      .map(write)
      .foreach(out.writeRow)
  }

}

/**
  * For app where single transformation is not enough.
  * Output rows may be emitted in any place, not just as an output of transformation.
  * Useful when there are multiple outputs for single input and output depends on internal state.
  */
trait CsvMultiProcessorApp extends CsvProcessorApp {
  def consume(in: Iterator[In], out: Out => Unit): Unit

  def process(in: CSVReader, out: CSVWriter): Unit = {
    consume(in.iterator.map(read), o => out.writeRow(write(o)))
  }
}
