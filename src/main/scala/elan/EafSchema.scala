package elan

import java.net.URI
import java.time.ZonedDateTime

/**
  * @author Tomáš Kraut <tomas.kraut@matfyz.cz>
  */
case class AnnotationDocument(
                               //attributes
                               date: ZonedDateTime,
                               author: String,
                               version: String,
                               format: String = "3.0",
                               //elements
                               licences: Seq[License],
                               header: Header,
                               timeOrder: TimeOrder,
                               tiers: Seq[Tier],
                               linguisticTypes: Seq[LinguisticType],
                               locales: Seq[Locale],
                               languages: Seq[Language],
                               constraints: Seq[Constraint],
                               controlledVocabularies: Seq[ControlledVocabulary],
                               lexiconRefs: Seq[LexiconRef],
                               externalRefs: Seq[ExternalRef]
                             )

object AnnotationDocument {
  val schema =
    <xsd:element name="ANNOTATION_DOCUMENT">
      <xsd:complexType>
        <xsd:sequence>
          <xsd:element name="LICENSE" type="licenseType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="HEADER" type="headType"/>
          <xsd:element name="TIME_ORDER" type="timeType"/>
          <xsd:element name="TIER" type="tierType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="LINGUISTIC_TYPE" type="lingType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="LOCALE" type="localeType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="LANGUAGE" type="langType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="CONSTRAINT" type="constraintType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="CONTROLLED_VOCABULARY" type="convocType"
                       minOccurs="0" maxOccurs="unbounded"/>
          <xsd:element name="LEXICON_REF" type="lexRefType" minOccurs="0"
                       maxOccurs="unbounded"/>
          <xsd:element name="EXTERNAL_REF" type="extRefType" minOccurs="0"
                       maxOccurs="unbounded"/>
        </xsd:sequence>
        <xsd:attribute name="DATE" type="xsd:dateTime" use="required"/>
        <xsd:attribute name="AUTHOR" type="xsd:string" use="required"/>
        <xsd:attribute name="VERSION" type="xsd:string" use="required"/>
        <xsd:attribute name="FORMAT" type="xsd:string" use="optional" default="3.0"/>
      </xsd:complexType>
    </xsd:element>
  val example =
    <ANNOTATION_DOCUMENT AUTHOR="ELAN" DATE="2016-12-16T11:51:48+01:00" FORMAT="3.0"
                         VERSION="3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                         xsi:noNamespaceSchemaLocation="http://www.mpi.nl/tools/elan/EAFv3.0.xsd"></ANNOTATION_DOCUMENT>
}


case class License(licenseUrl: Option[URI], content: String)

object License {
  val schema = <xsd:complexType name="licenseType">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="LICENSE_URL" type="xsd:anyURI" use="optional"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
}

case class Header(
                   //attributes
                   mediaFile: Option[String],
                   timeUnits: Header.TimeUnit = Header.TimeUnit.milliseconds,
                   //elements
                   mediaDescriptors: Seq[MediaDescriptor],
                   linkedFileDescriptors: Seq[LinkedFileDescriptor],
                   properties: Seq[Property]
                 )

object Header {

  sealed abstract class TimeUnit

  object TimeUnit {
    def fromString(s: String): TimeUnit = s match {
      case "NTSC-frames"  => `NTSC-frames`
      case "PAL-frames"   => `PAL-frames`
      case "milliseconds" => milliseconds
    }


    case object `NTSC-frames` extends TimeUnit

    case object `PAL-frames` extends TimeUnit

    case object milliseconds extends TimeUnit

  }

  val schema = <xsd:complexType name="headType">
    <xsd:sequence>
      <xsd:element name="MEDIA_DESCRIPTOR" minOccurs="0"
                   maxOccurs="unbounded">
        .....
      </xsd:element>
      <xsd:element name="LINKED_FILE_DESCRIPTOR" minOccurs="0"
                   maxOccurs="unbounded">
        .....
      </xsd:element>
      <xsd:element name="PROPERTY" type="propType" minOccurs="0"
                   maxOccurs="unbounded"/>
    </xsd:sequence>
    <xsd:attribute name="MEDIA_FILE" use="optional" type="xsd:string">
    </xsd:attribute>
    <xsd:attribute name="TIME_UNITS" use="optional" default="milliseconds">
      <xsd:simpleType>
        <xsd:restriction base="xsd:string">
          <xsd:enumeration value="NTSC-frames"/>
          <xsd:enumeration value="PAL-frames"/>
          <xsd:enumeration value="milliseconds"/>
        </xsd:restriction>
      </xsd:simpleType>
    </xsd:attribute>
  </xsd:complexType>
}

case class MediaDescriptor(
                            mediaUrl: URI,
                            relativeMediaUrl: Option[URI],
                            mimeType: String,
                            timeOrigin: Option[Long],
                            extractedFrom: Option[URI]
                          )

object MediaDescriptor {
  val schema = <xsd:element name="MEDIA_DESCRIPTOR" minOccurs="0" maxOccurs="unbounded">
    <xsd:complexType>
      <xsd:attribute name="MEDIA_URL" type="xsd:anyURI" use="required"/>
      <xsd:attribute name="RELATIVE_MEDIA_URL" type="xsd:anyURI" use="optional"/>
      <xsd:attribute name="MIME_TYPE" type="xsd:string" use="required"/>
      <xsd:attribute name="TIME_ORIGIN" type="xsd:long" use="optional"/>
      <xsd:attribute name="EXTRACTED_FROM" type="xsd:anyURI" use="optional"/>
    </xsd:complexType>
  </xsd:element>
}

case class LinkedFileDescriptor(
                                 linkUrl: URI,
                                 relativeLinkUrl: Option[URI],
                                 mimeType: String,
                                 timeOrigin: Option[Long],
                                 associatedWith: Option[URI]
                               )

object LinkedFileDescriptor {
  val schema = <xsd:element name="LINKED_FILE_DESCRIPTOR" minOccurs="0" maxOccurs="unbounded">
    <xsd:complexType>
      <xsd:attribute name="LINK_URL" type="xsd:anyURI" use="required"/>
      <xsd:attribute name="RELATIVE_LINK_URL" type="xsd:anyURI" use="optional"/>
      <xsd:attribute name="MIME_TYPE" type="xsd:string" use="required"/>
      <xsd:attribute name="TIME_ORIGIN" type="xsd:long" use="optional"/>
      <xsd:attribute name="ASSOCIATED_WITH" type="xsd:anyURI" use="optional"/>
    </xsd:complexType>
  </xsd:element>
}

case class Property(name: String, content: String) //TODO name optional? (see schema)

object Property {
  val schema = <xsd:complexType name="propType">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="NAME" type="xsd:string" use="optional"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
}

case class TimeOrder(timeSlots: Seq[TimeSlot]) extends AnyVal

object TimeOrder {
  val schema = <xsd:complexType name="timeType">
    <xsd:sequence>
      <xsd:element name="TIME_SLOT" minOccurs="0" maxOccurs="unbounded">
        <xsd:complexType>
          <xsd:attribute name="TIME_SLOT_ID" type="xsd:ID" use="required"/>
          <xsd:attribute name="TIME_VALUE" type="xsd:unsignedInt"
                         use="optional"/>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>

  val example = <TIME_ORDER>
    <TIME_SLOT TIME_SLOT_ID="ts1" TIME_VALUE="610"/>
    <TIME_SLOT TIME_SLOT_ID="ts2" TIME_VALUE="1950"/>
    <TIME_SLOT TIME_SLOT_ID="ts3" TIME_VALUE="1950"/>
    <TIME_SLOT TIME_SLOT_ID="ts4"/>
    <TIME_SLOT TIME_SLOT_ID="ts5"/>
    <TIME_SLOT TIME_SLOT_ID="ts6" TIME_VALUE="8420"/>
  </TIME_ORDER>
}

case class TimeSlot(
                     timeSlotId: String, //xsd:ID
                     timeValue: Option[Long] //xsd:unsignedInt
                   )

case class Tier(
                 //attributes
                 tierId: String,
                 participant: Option[String],
                 annotator: Option[String],
                 linguisticTypeRef: String,
                 defaultLocale: Option[String], //xsd:IDREF
                 parentRef: Option[String],
                 extRef: Option[String], //xsd:IDREF
                 langRef: Option[String], //xsd:IDREF
                 //elements
                 annotations: Seq[Annotation]
               )


object Tier {
  val schema = <xsd:complexType name="tierType">
    <xsd:sequence>
      <xsd:element name="ANNOTATION" type="annotationType" minOccurs="0"
                   maxOccurs="unbounded"/>
    </xsd:sequence>
    <xsd:attribute name="TIER_ID" type="xsd:string" use="required"/>
    <xsd:attribute name="PARTICIPANT" type="xsd:string" use="optional"/>
    <xsd:attribute name="ANNOTATOR" type="xsd:string" use="optional"/>
    <xsd:attribute name="LINGUISTIC_TYPE_REF" type="xsd:string" use="required"/>
    <xsd:attribute name="DEFAULT_LOCALE" type="xsd:IDREF" use="optional"/>
    <xsd:attribute name="PARENT_REF" type="xsd:string" use="optional"/>
    <xsd:attribute name="EXT_REF" type="xsd:IDREF" use="optional"/>
    <xsd:attribute name="LANG_REF" type="xsd:IDREF" use="optional"/>
  </xsd:complexType>
}

sealed abstract class Annotation {
  val annotationId: String //xsd:ID
  val extRef: Option[String] //xsd:IDREF
  val langRef: Option[String] //xsd:IDREF
  val cveRef: Option[String]
}

object Annotation {
  val schema = <xsd:complexType name="annotationType">
    <xsd:choice>
      <xsd:element name="ALIGNABLE_ANNOTATION" type="alignableType"/>
      <xsd:element name="REF_ANNOTATION" type="refAnnoType"/>
    </xsd:choice>
  </xsd:complexType>
    <xsd:attributeGroup name="annotationAttribute">
      <xsd:attribute name="ANNOTATION_ID" type="xsd:ID" use="required"/>
      <xsd:attribute name="EXT_REF" type="xsd:IDREFS" use="optional"/>
      <xsd:attribute name="LANG_REF" type="xsd:IDREF" use="optional"/>
      <xsd:attribute name="CVE_REF" type="xsd:string" use="optional"/>
    </xsd:attributeGroup>

  case class AlignableAnnotation(
                                  //attributes for annotation
                                  annotationId: String,
                                  extRef: Option[String],
                                  langRef: Option[String],
                                  cveRef: Option[String],
                                  //attributes for alignable
                                  timeSlotRef1: String, //xsd:IDREF
                                  timeSlotRef2: String, //xsd:IDREF
                                  svgRef: Option[String],
                                  //content
                                  annotationValue: AnnotationValue
                                ) extends Annotation

  object AlignableAnnotation {
    val schema = <xsd:complexType name="alignableType">
      <xsd:sequence>
        <xsd:element name="ANNOTATION_VALUE" type="xsd:string"/>
      </xsd:sequence>
      <xsd:attributeGroup ref="annotationAttribute"/>
      <xsd:attribute name="TIME_SLOT_REF1" type="xsd:IDREF" use="required"/>
      <xsd:attribute name="TIME_SLOT_REF2" type="xsd:IDREF" use="required"/>
      <xsd:attribute name="SVG_REF" type="xsd:string" use="optional"/>
    </xsd:complexType>
  }

  case class RefAnnotation(
                            //attributes for annotation
                            annotationId: String,
                            extRef: Option[String],
                            langRef: Option[String],
                            cveRef: Option[String],
                            //attributes for ref
                            annotationRef: String, //xsd:IDREF
                            previousAnnotation: Option[String], //xsd:IDREF
                            //content
                            annotationValue: AnnotationValue
                          ) extends Annotation


  object RefAnnotation {
    val schema = <xsd:complexType name="refAnnoType">
      <xsd:sequence>
        <xsd:element name="ANNOTATION_VALUE" type="xsd:string"/>
      </xsd:sequence>
      <xsd:attributeGroup ref="annotationAttribute"/>
      <xsd:attribute name="ANNOTATION_REF" type="xsd:IDREF" use="required"/>
      <xsd:attribute name="PREVIOUS_ANNOTATION" type="xsd:IDREF" use="optional"/>
    </xsd:complexType>
  }

}

case class AnnotationValue(content: String) extends AnyVal

case class LinguisticType(
                           linguisticTypeId: String,
                           timeAlignable: Option[Boolean], //TODO is Option needed?
                           constraints: Option[String], //xsd:IDREF
                           graphicReferences: Option[Boolean], //TODO is Option needed?
                           controlledVocabularyRef: Option[String],
                           extRef: Option[String], //xsd:IDREF
                           lexiconRef: Option[String] //xsd:IDREF
                         )

object LinguisticType {
  val schema = <xsd:complexType name="lingType">
    <xsd:attribute name="LINGUISTIC_TYPE_ID" type="xsd:string" use="required"/>
    <xsd:attribute name="TIME_ALIGNABLE" type="xsd:boolean" use="optional"/>
    <xsd:attribute name="CONSTRAINTS" type="xsd:IDREF" use="optional"/>
    <xsd:attribute name="GRAPHIC_REFERENCES" type="xsd:boolean" use="optional"/>
    <xsd:attribute name="CONTROLLED_VOCABULARY_REF" type="xsd:string" use="optional"/>
    <xsd:attribute name="EXT_REF" type="xsd:IDREF" use="optional"/>
    <xsd:attribute name="LEXICON_REF" type="xsd:IDREF" use="optional"/>
  </xsd:complexType>
}

case class Constraint(
                       stereotype: String, //xsd:ID
                       description: Option[String] //always Some
                     )

object Constraint {
  val schema = <xsd:complexType name="constraintType">
    <xsd:attribute name="STEREOTYPE" type="xsd:ID" use="required"/>
    <xsd:attribute name="DESCRIPTION" type="xsd:string" use="optional"/>
  </xsd:complexType>
  val TimeSubdivision = Constraint(
    stereotype = "Time_Subdivision",
    description = Some("Time subdivision of parent annotation's time interval, no time gaps allowed within this interval")
  )
  val SymbolicSubdivision = Constraint(
    stereotype = "Symbolic_Subdivision",
    description = Some("Symbolic subdivision of a parent annotation. Annotations refering to the same parent are ordered")
  )
  val symbolicAssociation = Constraint(
    stereotype = "Symbolic_Association",
    description = Some("1-1 association with a parent annotation")
  )
  val IncludedIn = Constraint(
    stereotype = "Included_In",
    description = Some("Time alignable annotations within the parent annotation's time interval, gaps are allowed")
  )

}

case class ControlledVocabulary(
                                 //attributes
                                 cvId: String,
                                 extRef: Option[String], //xsd:IDREF
                                 //elements
                                 descriptions: Seq[Description],
                                 cvEntriesMl: Seq[CvEntryMl]
                               )

object ControlledVocabulary {
  val schema = <xsd:complexType name="convocType">
    <xsd:sequence>
      <xsd:element name="DESCRIPTION" type="descMultiLangType" minOccurs="0"
                   maxOccurs="unbounded"/>
      <xsd:element name="CV_ENTRY_ML" type="cventryType" minOccurs="0"
                   maxOccurs="unbounded"/>
    </xsd:sequence>
    <xsd:attribute name="CV_ID" type="xsd:string" use="required"/>
    <xsd:attribute name="EXT_REF" type="xsd:IDREF" use="optional">
      <xsd:annotation>
        <xsd:documentation>
          A reference to an url of an external Controlled Vocabulary.
          Is intended to be mutually exclusive with a sequence of CV_ENTRY_ML elements.
        </xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
  </xsd:complexType>
}

case class CvEntryMl(
                      //attributes
                      cveId: String,
                      extRef: Option[String], //xsd:IDREF
                      //elements
                      cveValues: Seq[CveValue]
                    )

object CvEntryMl {
  val schema = <xsd:complexType name="cventryType">
    <xsd:sequence>
      <xsd:element name="CVE_VALUE" type="cveValueType" maxOccurs="unbounded"/>
    </xsd:sequence>
    <xsd:attribute name="CVE_ID" type="xsd:string" use="required"/>
    <xsd:attribute name="EXT_REF" type="xsd:IDREF" use="optional"/>
  </xsd:complexType>
}

case class CveValue(
                     //attributes
                     langRef: String,
                     description: Option[String],
                     //content
                     content: String
                   )

object CveValue {
  val schema = <xsd:complexType name="cveValueType">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="LANG_REF" type="xsd:IDREF" use="required"/>
        <xsd:attribute name="DESCRIPTION" type="xsd:string" use="optional"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
}

case class Description(
                        //attributes
                        langRef: String, //xsd:IDREF
                        //content
                        content: String
                      )

object Description {
  val schema = <xsd:complexType name="descMultiLangType">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="LANG_REF" type="xsd:IDREF" use="required"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
}

case class ExternalRef(
                        extRefId: String, //xsd:ID
                        `type`: ExternalRef.Type,
                        value: String
                      )

object ExternalRef {
  val schema = <xsd:complexType name="extRefType">
    <xsd:attribute name="EXT_REF_ID" type="xsd:ID" use="required"/>
    <xsd:attribute name="TYPE" use="required">
      <xsd:simpleType>
        <xsd:restriction base="xsd:string">
          <xsd:enumeration value="iso12620">
            <xsd:annotation>
              <xsd:documentation>A reference to the id of an ISO Data Category (url including id).
              </xsd:documentation>
            </xsd:annotation>
          </xsd:enumeration>
          <xsd:enumeration value="ecv">
            <xsd:annotation>
              <xsd:documentation>
                A reference to an external (closed) Controlled Vocabulary (url).
              </xsd:documentation>
            </xsd:annotation>
          </xsd:enumeration>
          <xsd:enumeration value="cve_id">
            <xsd:annotation>
              <xsd:documentation>
                A reference to the id of an Entry in an external Controlled Vocabulary (id).
              </xsd:documentation>
            </xsd:annotation>
          </xsd:enumeration>
          <xsd:enumeration value="lexen_id">
            <xsd:annotation>
              <xsd:documentation>
                A reference to the id of an entry in a lexicon (url, url+id or id)
              </xsd:documentation>
            </xsd:annotation>
          </xsd:enumeration>
          <xsd:enumeration value="resource_url">
            <xsd:annotation>
              <xsd:documentation>
                A reference or hyperlink to any type document (url)
              </xsd:documentation>
            </xsd:annotation>
          </xsd:enumeration>
          <!-- other external reference types can be added later -->
        </xsd:restriction>
      </xsd:simpleType>
    </xsd:attribute>
    <xsd:attribute name="VALUE" type="xsd:string" use="required"/>
  </xsd:complexType>

  sealed abstract class Type(val documentation: String)

  object Type {

    case object iso12620 extends Type("A reference to the id of an ISO Data Category (url including id).")

    case object ecv extends Type("A reference to an external (closed) Controlled Vocabulary (url).")

    case object ecv_id extends Type("A reference to the id of an Entry in an external Controlled Vocabulary (id).")

    case object lexen_id extends Type("A reference to the id of an entry in a lexicon (url, url+id or id)")

    case object resource_url extends Type("A reference or hyperlink to any type document (url)")

    case class Other(value: String) extends Type("other external reference types can be added later ")

    def fromString(str: String): Type = str match {
      case "iso12620"     => iso12620
      case "ecv"          => ecv
      case "ecv_id"       => ecv_id
      case "lexen_id"     => lexen_id
      case "resource_url" => resource_url
      case other          => Other(other)
    }
  }

}

case class Locale(
                   languageCode: String, //xsd:ID
                   countryCode: Option[String],
                   variant: Option[String]
                 )

object Locale {
  val schema = <xsd:complexType name="localeType">
    <xsd:attribute name="LANGUAGE_CODE" type="xsd:ID" use="required"/>
    <xsd:attribute name="COUNTRY_CODE" type="xsd:string" use="optional"/>
    <xsd:attribute name="VARIANT" type="xsd:string" use="optional"/>
  </xsd:complexType>
}

case class Language(
                     langId: String, //xsd:ID
                     langDef: Option[String],
                     langLabel: Option[String]
                   )

object Language {
  val schema = <xsd:complexType name="langType">
    <xsd:attribute name="LANG_ID" type="xsd:ID" use="required"/>
    <!-- definition is optional so that user defined languages are easy to add -->
    <xsd:attribute name="LANG_DEF" type="xsd:string" use="optional">
      <xsd:annotation>
        <xsd:documentation>
          ISO-639-3 still seems to be the best choice for language codes and closest to persistent
          language ID's seem to be the http://cdb.iso.org/lg/... identifiers also used by the iso-language-639-3 component
          in the CLARIN ComponentRegistry.
        </xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute name="LANG_LABEL" type="xsd:string" use="optional"/>
  </xsd:complexType>
}

case class LexiconRef(
                       lexRefId: String, //xsd:ID
                       name: String,
                       `type`: String,
                       url: String,
                       lexiconId: String,
                       lexiconName: String,
                       datcatId: Option[String],
                       datcatName: Option[String]
                     )

object LexiconRef {
  val schema = <xsd:complexType name="lexRefType">
    <xsd:attribute name="LEX_REF_ID" type="xsd:ID" use="required"/>
    <xsd:attribute name="NAME" type="xsd:string" use="required"/>
    <xsd:attribute name="TYPE" type="xsd:string" use="required"/>
    <xsd:attribute name="URL" type="xsd:string" use="required"/>
    <xsd:attribute name="LEXICON_ID" type="xsd:string" use="required"/>
    <xsd:attribute name="LEXICON_NAME" type="xsd:string" use="required"/>
    <xsd:attribute name="DATCAT_ID" type="xsd:string" use="optional"/>
    <xsd:attribute name="DATCAT_NAME" type="xsd:string" use="optional"/>
  </xsd:complexType>
}

//TODO REF_LINK_SET, CROSS_REF_LINK, GROUP_REF_LINK
