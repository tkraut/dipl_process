package elan

import java.net.URI
import java.time.ZonedDateTime

import convert.Xml._
import elan.Annotation.{AlignableAnnotation, RefAnnotation}
import elan.Header.TimeUnit

import scala.xml.Elem

/**
  * @author Tomáš Kraut <tomas.kraut@matfyz.cz>
  */
object EafXmlSerialization {
  private def attr(e: Elem, attr: String): String =
    e.attribute(attr).get.head.text

  private def optionalAttr(e: Elem, attr: String): Option[String] =
    for {
      nodes <- e.attribute(attr)
      first <- nodes.headOption
    } yield first.text


  implicit object AnnotationDocumentXml extends Serializable[AnnotationDocument] {
    override def fromXml(e: Elem): AnnotationDocument = AnnotationDocument(
      ZonedDateTime.parse(attr(e, "DATE")),
      attr(e, "AUTHOR"),
      attr(e, "VERSION"),
      optionalAttr(e, "FORMAT").getOrElse("3.0"),
      (e \ "LICENSE").map {
        case node: Elem => LicenseXml.fromXml(node)
      },
      (e \ "HEADER").head match {
        case node: Elem => HeaderXml.fromXml(node)
      },
      (e \ "TIME_ORDER").head match {
        case node: Elem => TimeOrderXml.fromXml(node)
      },
      (e \ "TIER").map {
        case node: Elem => TierXml.fromXml(node)
      },
      (e \ "LINGUISTIC_TYPE").map {
        case node: Elem => LinguisticTypeXml.fromXml(node)
      },
      (e \ "LOCALE").map {
        case node: Elem => LocaleXml.fromXml(node)
      },
      (e \ "LANGUAGE").map {
        case node: Elem => LanguageXml.fromXml(node)
      },
      (e \ "CONSTRAINT").map {
        case node: Elem => ConstraintXml.fromXml(node)
      },
      (e \ "CONTROLLED_VOCABULARY").map {
        case node: Elem => ControlledVocabularyXml.fromXml(node)
      },
      (e \ "LEXICON_REF").map {
        case node: Elem => LexiconRefXml.fromXml(node)
      },
      (e \ "EXTERNAL_REF").map {
        case node: Elem => ExternalRefXml.fromXml(node)
      },
    )

    override def toXml(t: AnnotationDocument): Elem =
      <ANNOTATION_DOCUMENT DATE={t.date.toString} AUTHOR={t.author} VERSION={t.version} FORMAT={t.format}>
        {t.licences.map(LicenseXml.toXml)}
        {HeaderXml.toXml(t.header)}
        {TimeOrderXml.toXml(t.timeOrder)}
        {t.tiers.map(TierXml.toXml)}
        {t.linguisticTypes.map(LinguisticTypeXml.toXml)}
        {t.locales.map(LocaleXml.toXml)}
        {t.languages.map(LanguageXml.toXml)}
        {t.constraints.map(ConstraintXml.toXml)}
        {t.controlledVocabularies.map(ControlledVocabularyXml.toXml)}
        {t.lexiconRefs.map(LexiconRefXml.toXml)}
        {t.externalRefs.map(ExternalRefXml.toXml)}
      </ANNOTATION_DOCUMENT>
  }

  implicit object LicenseXml extends Serializable[License] {
    override def fromXml(e: Elem): License = License(
      optionalAttr(e, "LICENSE_URL").map(URI.create),
      e.text
    )

    override def toXml(t: License): Elem =
      <LICENSE LICENSE_URL={t.licenseUrl.map(_.toString).orNull}>
        {t.content}
      </LICENSE>
  }

  implicit object HeaderXml extends Serializable[Header] {
    override def fromXml(e: Elem): Header = Header(
      optionalAttr(e, "MEDIA_FILE"),
      optionalAttr(e, "TIME_UNITS").map(TimeUnit.fromString).getOrElse(TimeUnit.milliseconds),
      (e \ "MEDIA_DESCRIPTOR").map {
        case node: Elem => MediaDescriptorXml.fromXml(node)
      },
      (e \ "LINKED_FILE_DESCRIPTOR").map {
        case node: Elem => LinkedFileDescriptorXml.fromXml(node)
      },
      (e \ "PROPERTY").map {
        case node: Elem => PropertyXml.fromXml(node)
      },
    )

    override def toXml(t: Header): Elem =
      <HEADER MEDIA_FILE={t.mediaFile.orNull} TIME_UNITS={t.timeUnits.toString}>
        {t.mediaDescriptors.map(MediaDescriptorXml.toXml)}
        {t.linkedFileDescriptors.map(LinkedFileDescriptorXml.toXml)}
        {t.properties.map(PropertyXml.toXml)}
      </HEADER>
  }

  implicit object TimeOrderXml extends Serializable[TimeOrder] {
    override def fromXml(e: Elem): TimeOrder = TimeOrder(
      (e \ "TIME_SLOT").map {
        case node: Elem => TimeSlotXml.fromXml(node)
      }
    )

    override def toXml(t: TimeOrder): Elem =
      <TIME_ORDER>
        {t.timeSlots.map(TimeSlotXml.toXml)}
      </TIME_ORDER>
  }

  implicit object TierXml extends Serializable[Tier] {
    override def fromXml(e: Elem): Tier = Tier(
      attr(e, "TIER_ID"),
      optionalAttr(e, "PARTICIPANT"),
      optionalAttr(e, "ANNOTATOR"),
      attr(e, "LINGUISTIC_TYPE_REF"),
      optionalAttr(e, "DEFAULT_LOCALE"),
      optionalAttr(e, "PARENT_REF"),
      optionalAttr(e, "EXT_REF"),
      optionalAttr(e, "LANG_REF"),
      (e \ "ANNOTATION").map {
        case node: Elem => AnnotationXml.fromXml(node)
      }
    )

    override def toXml(t: Tier): Elem =
      <TIER
        TIER_ID={t.tierId}
        PARTICIPANT={t.participant.orNull}
        ANNOTATOR={t.annotator.orNull}
        LINGUISTIC_TYPE_REF={t.linguisticTypeRef}
        DEFAULT_LOCALE={t.defaultLocale.orNull}
        PARENT_REF={t.parentRef.orNull}
        EXT_REF={t.extRef.orNull}
        LANG_REF={t.langRef.orNull}
      >
        {t.annotations.map(AnnotationXml.toXml)}
      </TIER>
  }

  implicit object LinguisticTypeXml extends Serializable[LinguisticType] {
    override def fromXml(e: Elem): LinguisticType = LinguisticType(
      attr(e, "LINGUISTIC_TYPE_ID"),
      optionalAttr(e, "TIME_ALIGNABLE").map(_.toBoolean),
      optionalAttr(e, "CONSTRAINTS"),
      optionalAttr(e, "GRAPHIC_REFERENCES").map(_.toBoolean),
      optionalAttr(e, "CONTROLLED_VOCABULARY_REF"),
      optionalAttr(e, "EXT_REF"),
      optionalAttr(e, "LEXICON_REF"),
    )

    override def toXml(t: LinguisticType): Elem =
      <LINGUISTIC_TYPE
        LINGUISTIC_TYPE_ID={t.linguisticTypeId}
        TIME_ALIGNABLE={t.timeAlignable.map(_.toString).orNull}
        CONSTRAINTS={t.constraints.orNull}
        GRAPHIC_REFERENCES={t.graphicReferences.map(_.toString).orNull}
        CONTROLLED_VOCABULARY_REF={t.controlledVocabularyRef.orNull}
        EXT_REF={t.extRef.orNull}
        LEXICON_REF={t.lexiconRef.orNull}
      >
      </LINGUISTIC_TYPE>
  }

  implicit object LocaleXml extends Serializable[Locale] {
    override def fromXml(e: Elem): Locale = Locale(
      attr(e, "LANGUAGE_CODE"),
      optionalAttr(e, "COUNTRY_CODE"),
      optionalAttr(e, "VARIANT"),
    )

    override def toXml(t: Locale): Elem =
      <LOCALE LANGUAGE_CODE={t.languageCode} COUNTRY_CODE={t.countryCode.orNull} VARIANT={t.variant.orNull}>
      </LOCALE>
  }

  implicit object LanguageXml extends Serializable[Language] {
    override def fromXml(e: Elem): Language = Language(
      attr(e, "LANG_ID"),
      optionalAttr(e, "LANG_DEF"),
      optionalAttr(e, "LANG_LABEL"),
    )

    override def toXml(t: Language): Elem =
      <LANGUAGE LANG_ID={t.langId} LANG_DEF={t.langDef.orNull} LANG_LABEL={t.langLabel.orNull}>
      </LANGUAGE>
  }

  implicit object ConstraintXml extends Serializable[Constraint] {
    override def fromXml(e: Elem): Constraint = Constraint(
      attr(e, "STEREOTYPE"),
      optionalAttr(e, "DESCRIPTION"),
    )

    override def toXml(t: Constraint): Elem =
      <CONSTRAINT STEREOTYPE={t.stereotype} DESCRIPTION={t.description.orNull}></CONSTRAINT>
  }

  implicit object ControlledVocabularyXml extends Serializable[ControlledVocabulary] {
    override def fromXml(e: Elem): ControlledVocabulary = ControlledVocabulary(
      attr(e, "CV_ID"),
      optionalAttr(e, "EXT_REF"),
      (e \ "DESCRIPTION").map {
        case node: Elem => DescriptionXml.fromXml(node)
      },
      (e \ "CV_ENTRY_ML").map {
        case node: Elem => CvEntryMlXml.fromXml(node)
      },
    )

    override def toXml(t: ControlledVocabulary): Elem =
      <CONTROLLED_VOCABULARY CV_ID={t.cvId} EXT_REF={t.extRef.orNull}>
        {t.descriptions.map(DescriptionXml.toXml)}
        {t.cvEntriesMl.map(CvEntryMlXml.toXml)}
      </CONTROLLED_VOCABULARY>
  }

  implicit object LexiconRefXml extends Serializable[LexiconRef] {
    override def fromXml(e: Elem): LexiconRef = LexiconRef(
      attr(e, "LEX_REF_ID"),
      attr(e, "NAME"),
      attr(e, "TYPE"),
      attr(e, "URL"),
      attr(e, "LEXICON_ID"),
      attr(e, "LEXICON_NAME"),
      optionalAttr(e, "DATCAT_ID"),
      optionalAttr(e, "DATCAT_NAME"),
    )

    override def toXml(t: LexiconRef): Elem = ???
  }

  implicit object ExternalRefXml extends Serializable[ExternalRef] {
    override def fromXml(e: Elem): ExternalRef = ExternalRef(
      attr(e, "EXT_REF_ID"),
      ExternalRef.Type.fromString(attr(e, "TYPE")),
      attr(e, "VALUE"),
    )

    override def toXml(t: ExternalRef): Elem = ???
  }

  implicit object MediaDescriptorXml extends Serializable[MediaDescriptor] {
    override def fromXml(e: Elem): MediaDescriptor = MediaDescriptor(
      URI.create(attr(e, "MEDIA_URL")),
      optionalAttr(e, "RELATIVE_MEDIA_URL").map(URI.create),
      attr(e, "MIME_TYPE"),
      optionalAttr(e, "TIME_ORIGIN").map(_.toLong),
      optionalAttr(e, "EXTRACTED_FROM").map(URI.create),
    )

    override def toXml(t: MediaDescriptor): Elem =
      <MEDIA_DESCRIPTOR
        MEDIA_URL={t.mediaUrl.toString}
        RELATIVE_MEDIA_URL={t.relativeMediaUrl.map(_.toString).orNull}
        MIME_TYPE={t.mimeType}
        TIME_ORIGIN={t.timeOrigin.map(_.toString).orNull}
        EXTRACTED_FROM={t.extractedFrom.map(_.toString).orNull}
      >
      </MEDIA_DESCRIPTOR>
  }

  implicit object LinkedFileDescriptorXml extends Serializable[LinkedFileDescriptor] {
    override def fromXml(e: Elem): LinkedFileDescriptor = LinkedFileDescriptor(
      URI.create(attr(e, "LINK_URL")),
      optionalAttr(e, "RELATIVE_LINK_URL").map(URI.create),
      attr(e, "MIME_TYPE"),
      optionalAttr(e, "TIME_ORIGIN").map(_.toLong),
      optionalAttr(e, "ASSOCIATED_WITH").map(URI.create),
    )

    override def toXml(t: LinkedFileDescriptor): Elem =
      <MEDIA_DESCRIPTOR
        MEDIA_URL={t.linkUrl.toString}
        RELATIVE_MEDIA_URL={t.relativeLinkUrl.map(_.toString).orNull}
        MIME_TYPE={t.mimeType}
        TIME_ORIGIN={t.timeOrigin.map(_.toString).orNull}
        EXTRACTED_FROM={t.associatedWith.map(_.toString).orNull}
      >
      </MEDIA_DESCRIPTOR>
  }

  implicit object PropertyXml extends Serializable[Property] {
    override def fromXml(e: Elem): Property = Property(
      attr(e, "NAME"),
      e.text
    )

    override def toXml(t: Property): Elem =
      <PROPERTY NAME={t.name}>
        {t.content}
      </PROPERTY>
  }

  implicit object TimeSlotXml extends Serializable[TimeSlot] {
    override def fromXml(e: Elem): TimeSlot = TimeSlot(
      attr(e, "TIME_SLOT_ID"),
      optionalAttr(e, "TIME_VALUE").map(_.toLong),
    )

    override def toXml(t: TimeSlot): Elem =
      <TIME_SLOT TIME_SLOT_ID={t.timeSlotId} TIME_VALUE={t.timeValue.map(_.toString).orNull}>
      </TIME_SLOT>
  }

  implicit object AnnotationXml extends Serializable[Annotation] {
    override def fromXml(e: Elem): Annotation =
      e.child.collectFirst {
        case aa: Elem if aa.label == "ALIGNABLE_ANNOTATION" => AlignableAnnotationXml.fromXml(aa)
        case ra: Elem if ra.label == "REF_ANNOTATION"       => RefAnnotationXml.fromXml(ra)
      }.get

    override def toXml(t: Annotation): Elem = t match {
      case a: AlignableAnnotation => AlignableAnnotationXml.toXml(a)
      case r: RefAnnotation => RefAnnotationXml.toXml(r)
    }
  }

  implicit object DescriptionXml extends Serializable[Description] {
    override def fromXml(e: Elem): Description = Description(
      attr(e, "LANG_REF"),
      e.text
    )

    override def toXml(t: Description): Elem =
      <DESCRIPTION LANG_REF={t.langRef}>{t.content}</DESCRIPTION>
  }

  implicit object CvEntryMlXml extends Serializable[CvEntryMl] {
    override def fromXml(e: Elem): CvEntryMl = CvEntryMl(
      attr(e, "CVE_ID"),
      optionalAttr(e, "EXT_REF"),
      (e \ "CVE_VALUE").map {
        case node: Elem => CveValueXml.fromXml(node)
      },
    )

    override def toXml(t: CvEntryMl): Elem =
      <CV_ENTRY_ML CVE_ID={t.cveId} EXT_REF={t.extRef.orNull}
      >{t.cveValues.map(CveValueXml.toXml)}</CV_ENTRY_ML>
  }

  implicit object AlignableAnnotationXml extends Serializable[AlignableAnnotation] {
    override def fromXml(e: Elem): AlignableAnnotation = AlignableAnnotation(
      attr(e, "ANNOTATION_ID"),
      optionalAttr(e, "EXT_REF"),
      optionalAttr(e, "LANG_REF"),
      optionalAttr(e, "CVE_REF"),
      attr(e, "TIME_SLOT_REF1"),
      attr(e, "TIME_SLOT_REF2"),
      optionalAttr(e, "SVG_REF"),
      (e \ "ANNOTATION_VALUE").head match {
        case node: Elem => AnnotationValueXml.fromXml(node)
      },
    )

    override def toXml(t: AlignableAnnotation): Elem =
      <ALIGNABLE_ANNOTATION
        ANNOTATION_ID={t.annotationId}
        EXT_REF={t.extRef.orNull}
        LANG_REF={t.langRef.orNull}
        CVE_REF={t.cveRef.orNull}
        TIME_SLOT_REF1={t.timeSlotRef1}
        TIME_SLOT_REF2={t.timeSlotRef2}
        SVG_REF={t.svgRef.orNull}
      >{AnnotationValueXml.toXml(t.annotationValue)}</ALIGNABLE_ANNOTATION>
  }

  implicit object RefAnnotationXml extends Serializable[RefAnnotation] {
    override def fromXml(e: Elem): RefAnnotation = RefAnnotation(
      attr(e, "ANNOTATION_ID"),
      optionalAttr(e, "EXT_REF"),
      optionalAttr(e, "LANG_REF"),
      optionalAttr(e, "CVE_REF"),
      attr(e, "ANNOTATION_REF"),
      optionalAttr(e, "PREVIOUS_ANNOTATION"),
      (e \ "ANNOTATION_VALUE").head match {
        case node: Elem => AnnotationValueXml.fromXml(node)
      },
    )

    override def toXml(t: RefAnnotation): Elem =
      <ALIGNABLE_ANNOTATION
        ANNOTATION_ID={t.annotationId}
        EXT_REF={t.extRef.orNull}
        LANG_REF={t.langRef.orNull}
        CVE_REF={t.cveRef.orNull}
        ANNOTATION_REF={t.annotationRef}
        PREVIOUS_ANNOTATION={t.previousAnnotation.orNull}
      >
        {AnnotationValueXml.toXml(t.annotationValue)}
      </ALIGNABLE_ANNOTATION>
  }

  implicit object CveValueXml extends Serializable[CveValue] {
    override def fromXml(e: Elem): CveValue = CveValue(
      attr(e, "LANG_REF"),
      optionalAttr(e, "DESCRIPTION"),
      e.text
    )

    override def toXml(t: CveValue): Elem =
      <CVE_VALUE LANG_REF={t.langRef} DESCRIPTION={t.description.orNull}
      >{t.content}</CVE_VALUE>
  }

  implicit object AnnotationValueXml extends Serializable[AnnotationValue] {
    override def fromXml(e: Elem): AnnotationValue = AnnotationValue(
      e.text
    )

    override def toXml(t: AnnotationValue): Elem =
      <ANNOTATION_VALUE>{t.content}</ANNOTATION_VALUE>
  }

}
