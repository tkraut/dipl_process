package convert

import scala.xml.Elem

object Xml {

  trait ToXml[T] {
    def toXml(t: T): Elem
  }

  trait FromXml[T] {
    def fromXml(e: Elem): T
  }

  trait Serializable[T] extends FromXml[T] with ToXml[T]

  implicit class ToXmlWrapper[T: ToXml](t: T) {
    def toXml: Elem = implicitly[ToXml[T]].toXml(t)
  }

  implicit class FromXmlWrapper[T: FromXml](e: Elem) {
    def fromXml: T = implicitly[FromXml[T]].fromXml(e)
  }

}
