import elan.EafXmlSerialization.AnnotationDocumentXml

import scala.xml.XML

/**
  * @author Tomáš Kraut <tomas.kraut@matfyz.cz>
  */
object convertRun {
  def main(args: Array[String]): Unit = {
    val xml = XML.loadFile(args(0))
    val eaf = AnnotationDocumentXml.fromXml(xml)
    val rt = AnnotationDocumentXml.toXml(eaf)
    print(rt)
  }
}
